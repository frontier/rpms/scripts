#!/usr/bin/env python
import sys, os, time, datetime

idStart={} 
idEnd={} 
idUsers={} 
idUserTables={} 
idDuration={} 
userTableDuration ={} 

# TBD: sorts to be runnable from another application

def columnSort( fileName, columnNumber):
	os.system('column -t '+ fileName+ '>'+fileName + '_')
	os.system('/bin/mv -f '+fileName+ '_ '+ fileName)	
	os.system('sort -k'+ str(columnNumber) + ' -n -r '+ fileName+ '>'+fileName+ '_')
	os.system('/bin/mv -f '+ fileName+ '_ '+ fileName)	
	
def isIdStart( line):
	return line.find("DB connection acquired")>=0
 
def isIdEnd( line):
	return line.find("DB connection released")>=0
 
def isQuery( line):
	return line.upper().find("SELECT") and line.upper().find("FROM")>=0 


def getTimeDate( line):
 	timeDateStr1=  line.split()[1:3]
	timeDateStr1[1]= timeDateStr1[1].split('.')[0]
	timeDateStr=  str(timeDateStr1)
	timeDate=  datetime.datetime(*time.strptime(timeDateStr,"['%m/%d/%y', '%H:%M:%S']")[0:6]) 
	#timeDate+= datetime.timedelta(hours=int(line.split()[4])/100) # time zone correction. Wrong!
	#print 'timeDate:',timeDate
	return timeDate

if __name__ == '__main__':
	if len(sys.argv)>1:
		logFileName= sys.argv[1]
	else:
		logFileName='/data/tomcat_logs/catalina.out'
		logFileName='./logs/catalina.out.1' #'./logs/part' #TBD: comment out
		logFileName='./logs/part' #TBD: comment out
	if len(sys.argv)>2:
		resultsFileName= sys.argv[2]
	else:
		resultsFileName= './logAnalysisResults'
	
	log = open(logFileName)
	i=0
	maxLinesAnalyze=0 #0  means unlimited
	for line in log:
		if maxLinesAnalyze>0 and i>=maxLinesAnalyze:
			print 'analyzed ',i,' lines'
			break 
		i+=1
		splitedLine= line.split()
		#print 'i:',i,'line:',line #,'splitedLine:',splitedLine,'len:',len(splitedLine)
		if len(splitedLine)>5:
			splitedLine5eq=splitedLine[5].split('=')
			if len(splitedLine5eq)>1:
				id= splitedLine5eq[1]
				#print 'id:',id
		if isIdStart( line):
			timeDate= getTimeDate( line)
			idStart[ id]= timeDate 
			#print 'id:',id,'startTime:',timeDate,'len(idStart)',len(idStart)		
			continue
		if isIdEnd( line):
			# Keep: end time, duration
			timeDate= getTimeDate( line)
			idEnd[ id]= timeDate 
			#print 'id:',id,'endTime:',timeDate
			if idStart.has_key( id):
				hours,minutes,seconds= str(idEnd[id]-idStart[id]).split(':')
				duration= int(seconds)+ 60* int(minutes)+ 3600*int(hours)
				idDuration[ id]= str(duration)

				# Fill userTableDuration
				if idUserTables.has_key( id):
					for userTable in idUserTables[id]:
						if userTableDuration.has_key( userTable):
							userTableDuration[userTable]+= duration
						else:
							userTableDuration[userTable]=duration
			continue
		if isQuery( line):
			# keep its user and tableName
			fromIndex= line.upper().find( 'FROM')
			if fromIndex<0:
				print "ERROR: fromIndex<0 for line:",line
				sys.exit(0)
			userTableList= line[fromIndex:].split()[1].split('.')
			if len(userTableList)<2:
				if userTableList[0].find('ALL_')== 0 or userTableList[0].upper()=='DUAL]' or  userTableList[0]== 'raddr' or userTableList[0]=='FROM': # excused
					continue
				print 'ERROR: len(userTableList)<2:', userTableList
				sys.exit(0)
				continue
			user, table= userTableList
			user_table= user+' '+table
			#print 'userTable:',userTable, 'user:', user, 'table:', table
			if idUsers.has_key(id):
				idUsers[id].add(user)
			else:
				idUsers[id]= set([user])
			if idUserTables.has_key(user_table):
				idUserTables[id].add(user_table)
			else:
				idUserTables[id]= set([user_table])
			#print 'id:',id,'user_table:',user_table
			continue

	# Write results to file: id startTime endTime duration user table
	results= open(resultsFileName, 'w')
	#header1='1-id 2-fromDate 3-fromTime 4-untilDate 5-untilTime 6-duration 7-user 8-table\n'
	#header2='= = = = = = = =\n'
	#results.write( header1)
	#results.write( header2)
	#print 'after open results file'
	#print 'len(idStart):',len(idStart)
	for id in idStart:
		#print 'id:',id
		if not idEnd.has_key(id):
			continue
		#print 'id:',id,'str(idStart[id])',str(idStart[id]),'str(idEnd[id])',str(idEnd[id]),'idDuration[id]',idDuration[id] #iiiiXX
		idAndTime= id+ ' '+str(idStart[id])+' '+str(idEnd[id])+' '+str(idDuration[id])
		#print 'idAndTime:',idAndTime
		if idUserTables.has_key(id):
			#print 'idUserTables.has_key'
			for userTable in idUserTables[id]:
				result= idAndTime+ ' '+ userTable+ '\n'
				#print 'result:', result
				results.write( result)
	results.close()
	columnSort( resultsFileName, 6)

	# create file with combined durations per user table
	# Assumption: each id has at the most one query
	fileName= 'userTableDuration'
	userTableDurationFile= open( fileName, 'w')
	for userTable in userTableDuration:
		userTableDurationFile.write( userTable+' '+ str(userTableDuration[userTable])+ '\n')
	userTableDurationFile.close()
	columnSort( fileName, 3)
