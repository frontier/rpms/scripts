#!/bin/sh 
# Usage:
# - In order to get 'file', using its input 'file.proto' file, do:
# - Write an input 'file.proto' file
#   where each variable to be substituted VAR_TO_BE_SUBSTITUTED is written as @@@VAR_TO_BE_SUBSTITUTED@@@.
# - export the vars to be substituted with the values to be substituted,
#   for example: example VAR_TO_BE_SUBSTITUTED=theActualValueNeededForVarToBeSubstituted
# - Run command: unproto.sh ...
#
# TBD: Consider: enhance this not to have to specify the args to be substituted

# Get args
if [ $# -lt 3 ]; then
	echo "Usage: $0 protoFile doEraseProto{0|1} mode VAR_TO_BE_SUBSTITUTED1 VAR_TO_BE_SUBSTITUTED2 ... VAR_TO_BE_SUBSTITUTEDn ]"
	exit
fi

protoFile=$1 ; shift
doEraseProto=$1 ; shift
mode_=$1 ; shift

# Find file out of protoFile:
#	- It should end by .proto
#	- Remove .proto from the name
fileName=`echo ${protoFile}|awk -F '.proto' '{print $1}'`
if [ ${fileName}".proto" != ${protoFile} ]; then
	echo "Arg protoFile should end with .proto, and not have .proto in another place at the file name"
fi

# Create a temporary unque file to unproto
unprotoTempFile=`mktemp /tmp/unprotoMeXXXXXX`

cp -f ${protoFile} ${unprotoTempFile}

# Do the substitutions
#echo "Args: $*"
for i in $*
do
	eval sedArgs='s+@@@'$i'@@@+$'$i'+g'
	#echo "sedArgs: ${sedArgs}"
	sed -i "${sedArgs}" ${unprotoTempFile}
done

# put unprotod content to the target location
install --mode=${mode_} ${unprotoTempFile} ${fileName}
rm -f ${unprotoTempFile}
if [ $doEraseProto -eq 1 ]; then
	rm -f ${protoFile}
fi

# Verify that no @@@ are left at unprotoed file
if [ ! `grep "@@@" ${fileName}|wc -l` -eq 0 ]; then
	echo "ERROR: unproto failed: @@@ does exist at ${fileName}:"
	cat ${fileName}
	exit 1
fi
