#!/bin/bash 
# cp rpm to web
# To be run from a .../rpms/<rpmSpecific>/scripts dir in order to be able
#    to find what rpm to refer to
# Optional parameters:
# -s rpm_source_directory destination_rpm_directory

rpmsDir=/home/dbfrontier/dist/rpms
sourceDir=$HOME/rpms

#rpmsDir=/tmp/rpms #For debugging

. ../../scripts/utilities.sh

# Set osSpecificList, osVersion, and rpmsDir
. ../../scripts/osSpecificRPMs.sh

if [ $# -gt 0 ]; then
    if [ "$1" == "-s" ]; then
	shift
	sourceDir=$1
	shift
    fi
    if [ $# -gt 0 ]; then
	rpmsDir=$1
    fi
    echo "rpmsDir: $rpmsDir"
else
    # Verify that the person that issues this command did not do this by mistake
    read -p "Are you sure that you want to write the rpm to the main repository?(no|yes): " sure
    #echo "sure: $sure"
    if [ X${sure} != Xyes ] ; then
	exit
    fi
fi

DirName="$(cd ..;basename `pwd`)"

cd ${sourceDir}
rpmsMainDir=.
touch ${rpmsMainDir}/doc/_README

# Creating a united document with documentation details of each rpm
README_COMBINED=${rpmsMainDir}/doc/_README_COMBINED

DEBUGINFORPM="${rpmsMainDir}/${DirName}/RPMS/${arch}/${Name}-debuginfo-${Version}-${Release}.${arch}.rpm"
if [ ! -f $DEBUGINFORPM ]; then
    DEBUGINFORPM=""
fi

standardRPM=${packageVersion}.${arch}.rpm
prefixRPMName=RPMS/${arch}/${packageVersion}
suffixRPMName=${arch}.rpm


targetRPM=${rpmsMainDir}/${DirName}/RPMS/${arch}/${standardRPM}
if [ ! -z "$osVersion" ]; then
    targetRPM2=${rpmsMainDir}/${DirName}/RPMS/${arch}/${packageVersion}.${osVersion}.${arch}.rpm
    targetRPM=${targetRPM2}
    TAR_FILES="${targetRPM}"
else
    cp ${rpmsMainDir}/frontier-release/SOURCES/cern-frontier.repo ${rpmsMainDir}/doc/
    TAR_FILES="${rpmsMainDir}/${DirName}/SRPMS/${packageVersion}.src.rpm  \
	    ${targetRPM} \
	    $DEBUGINFORPM \
	    ${rpmsMainDir}/${DirName}/doc/${Name}README \
	    ${rpmsMainDir}/${DirName}/doc/${Name}RELEASE_NOTES\
	    ${README_COMBINED} \
	    ${rpmsMainDir}/doc/cern-frontier.repo \
	    ${rpmsMainDir}/doc/frontierRpmInstallationError.txt \
	    ${rpmsMainDir}/doc/knownIssues.txt \
	    ${rpmsMainDir}/scripts/awstatsSiteProjectNodesMapping" 
fi

touch ${TAR_FILES}

cp -f ${rpmsMainDir}/doc/_README ${README_COMBINED}
echo>>${README_COMBINED}
echo "Per frontier rpm specific notes:">>${README_COMBINED}
echo>>${README_COMBINED}
# Manually excluding frontier-servlet
more ${rpmsMainDir}/frontier-awstats/doc/frontier-awstatsREADME ${rpmsMainDir}/frontier-squid/doc/frontier-squidREADME ${rpmsMainDir}/frontier-tomcat/doc/frontier-tomcatREADME | grep -v 'For common README for CERN frontier rpms' | grep -v "http://frontier.cern.ch/dist/rpms/_README"| grep -v 'cernFrontierRpmsREADME' | grep -v 'Specific for frontier-' | sed 's+/doc/.*++' | sed 's+$HOME/rpms/++' >> ${README_COMBINED}
#more ${rpmsMainDir}/*/doc/*E | grep -v 'For common README for CERN frontier rpms' | grep -v "http://frontier.cern.ch/dist/rpms/_README"| grep -v 'cernFrontierRpmsREADME' | grep -v 'Specific for frontier-' | sed 's+/doc/.*++' | sed 's+$HOME/rpms/++' >> ${README_COMBINED}

# replace the release notes with the rpm changelog if present
NOTES="`rpm -q --changelog -p ${rpmsMainDir}/${DirName}/RPMS/${arch}/${packageVersion}.${arch}.rpm 2>/dev/null`"
if [ -n "$NOTES" ] && [ "$NOTES" != "(none)" ]; then
    (echo "$NOTES"; echo) \
	> ${rpmsMainDir}/${DirName}/doc/${Name}RELEASE_NOTES
fi

TMP_FILE_NAME=tmp_rpm2web.$$.z
TMP_FILE=/tmp/${TMP_FILE_NAME}
trap "rm -f $TMP_FILE" 0
tar zcf ${TMP_FILE} ${TAR_FILES}

#	${rpmsMainDir}/doc/_README \

sudo -u dbfrontier scp -P 2222 ${TMP_FILE} frontier:${rpmsDir}/${TMP_FILE_NAME}
if [ $? -gt 0 ]; then
    echo "scp failed. Going to exit. Please run rpm2web.sh again"
    exit 0
fi

# - tar -x ${rpmsDir}/${TMP_FILE}
# - Update the cern-frontier yum repository

sudo -u dbfrontier ssh -p 2222 frontier bash -s - "${rpmsDir}" "${TMP_FILE_NAME}" "${README_COMBINED}" "${DirName}" "${prefixRPMName}" "${suffixRPMName}" "${osSpecificList}" < ./scripts/updaterepo.sh
