#!/bin/bash
# Run from a .../rpms/<rpm specific>/scripts dir to tag a release
# use: ../../scripts/gittag.sh

set -e

source ../../scripts/utilities.sh

set -x
git tag ${packageVersion}
git push --tags
