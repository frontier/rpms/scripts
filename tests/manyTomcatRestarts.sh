#!/bin/bash

export times=1000
echo "Going to cause frontier-tomcat to restart $times times by touching a servlet file"
#for i in {1..$times}
for (( i=1; i<=$times; i++ ))
do
	sudo -u dbfrontier touch /usr/share/dbfrontier/tomcat/conf/Catalina/localhost/atlr.xml
	#sleep 0.01
	#echo "did it for time $i"
	echo -n "$i "
done
echo
echo "to test: ~dfront/public/testFrontier" 
 ~dfront/public/testFrontier
