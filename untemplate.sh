#!/bin/sh 
# Usage:
# - In order to get 'file', using its input 'file.template' file, do:
# - Write an input 'file.template' file
#   where each variable to be substituted VAR_TO_BE_SUBSTITUTED is written as @@@VAR_TO_BE_SUBSTITUTED@@@.
# - export the vars to be substituted with the values to be substituted,
#   for example: example VAR_TO_BE_SUBSTITUTED=theActualValueNeededForVarToBeSubstituted
# - Run command: untemplate.sh templateFile VAR_TO_BE_SUBSTITUTED1 VAR_TO_BE_SUBSTITUTED2 ... VAR_TO_BE_SUBSTITUTEDn
#
# TBD: Consider: enhance this not to have to specify the args to be substituted

# Arguments: dirFileNameToBeUntemplated mode VAR_TO_BE_SUBSTITUTED1 VAR_TO_BE_SUBSTITUTED2 ... VAR_TO_BE_SUBSTITUTEDn

# Get args
if [ $# -lt 3 ]; then
	echo "Usage: $0 templateFile mode VAR_TO_BE_SUBSTITUTED1 VAR_TO_BE_SUBSTITUTED2 ... VAR_TO_BE_SUBSTITUTEDn ]"
	exit
fi

templateFile=$1
shift
mode_=$1
shift

# Find file out of templateFile:
#	- It should end by .template
#	- Remove .template from the name
fileName=`echo ${templateFile}|awk -F '.template' '{print $1}'`
if [ ${fileName}".template" != ${templateFile} ]; then
	echo "Arg templateFile should end with .template, and not have .template in another place at the file name"
fi

# Create a temporary unque file to untemplate
untemplateTempFile=`mktemp /tmp/untemplateMeXXXXXX`

cp -f ${templateFile} ${untemplateTempFile}

# Do the substitutions
#echo "Args: $*"
for i in $*
do
	#echo "going to eval: sed -i 's+@@@'$i'@@@+$'$i'+g' ${untemplateTempFile}"
	eval sed -i 's+@@@'$i'@@@+$'$i'+g' ${untemplateTempFile}
done

# put untemplated content to the target location
install ${mode} ${untemplateTempFile} ${fileName}
rm -f ${untemplateTempFile}

