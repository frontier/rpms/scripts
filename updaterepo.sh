#!/usr/bin/bash

rpmsDir=$1
TMP_FILE_NAME=$2
README_COMBINED=$3
DirName=$4
prefixRPMName=$5
suffixRPMName=$6
osSpecificList="$7"

standardRPM=${prefixRPMName}.${suffixRPMName}

set -ex
cd "${rpmsDir}"
test ! -d scripts || (echo "${rpmsDir}/scripts already exists, giving up" && exit 1)
tar zxf "./${TMP_FILE_NAME}"
test -f "${README_COMBINED}" && mv "${README_COMBINED}" ./_README 
rm -f tmp_* 
test -f ./scripts/awstatsSiteProjectNodesMapping && mv ./scripts/awstatsSiteProjectNodesMapping . 
cp -rpf ./"${DirName}"/* . 
rm -rf ./"${DirName}"/
test -d ./doc && cp -f ./doc/* .
rm -rf ./doc/ ./scripts
set +x
missingRPM=0
tag=
if [[ ${rpmsDir} == *"-debug" ]]; then
	tag="-debug"
fi
cd ..
if [ ! -z "$osSpecificList" ]; then
    for osTarget in $osSpecificList
    do
	rpmFile=${osTarget}${tag}/${prefixRPMName}.${osTarget}.${suffixRPMName}
	if [ ! -s "$rpmFile" ]; then
	    echo
	    echo "$rpmFile" is missing. Please build and release it.
	    missingRPM=1
	fi
    done
else
    # el9 is the only real directory. Later el7 or el8 could be added.
    for osTarget in el9
    do
	if [ ! -f ${osTarget}${tag}/${standardRPM} ]; then
	    pushd `dirname ${osTarget}${tag}/${standardRPM}`
	    ln -s ../../../rpms${tag}/${standardRPM} .
	    popd
	fi
    done
fi
# Check if all required RPMs are in place.
fullstandardRPM=rpms${tag}/${prefixRPMName}.${suffixRPMName}
if [ ${missingRPM} -eq 0 -a -s ${fullstandardRPM} ]; then
    (cd rpms${tag} ; createrepo .)
    (cd el9${tag} ; createrepo .)
elif [ ! -s "$fullstandardRPM" ]; then
    echo
    echo ${fullstandardRPM} is missing. Please build and release it.
fi
