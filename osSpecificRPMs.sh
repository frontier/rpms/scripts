#!/bin/bash 
# This script must be sourced to provide:
# osSpecificList, osVersion, and rpmsDir
# Input: "Name" must be set to the application name.

#### For new OS-specific RPMs, add statements here. osSpecificList should be a quoted, space-separated list.
osSpecificList=
case "${Name}" in 
    frontier-squid)
	osSpecificList="el9"
	;;
esac

osVersion=
if [ ! -z "$osSpecificList" ]; then
    source /etc/os-release
    # VERSION_ID=9  # For testing
    for osTarget in $osSpecificList
    do
	# Compare last character of osTarget with VERSION_ID number
	if [[ ${osTarget:2:1} == "${VERSION_ID:0:1}" ]] ; then
	    osVersion=${osTarget}
	    rpmsDir=/home/dbfrontier/dist/${osTarget}
	fi
  done
fi
