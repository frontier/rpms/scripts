#!/bin/bash
# The script builds an rpm 
# Run this from this directory, scripts,

if [ "$(basename $(pwd))" != "scripts" ]; then
    echo "`basename $0` must be run from individual package scripts directory" >&2
    exit 1
fi

set -x

# /usr/local/bin inserts bad dependencies, remove it from path
PATH="`echo $PATH|sed 's,:/usr/local/bin,,g'`"

# Assumption: ../SPEC has exactly one spec file
Name="`basename ../SPECS/*.spec .spec`"

# Check OS version and provide correct osVersion if needed. Requires "Name"
. ../../scripts/osSpecificRPMs.sh

mkdir -p /tmp/$USER

# Create a .macros file
topDir=$(dirname $(pwd))
mainTopDir=$(dirname $topDir)
rpmmacrosDir=`pwd`
rpmmacros=${rpmmacrosDir}/.rpmmacros
touch  ${rpmmacros}
mv -f ${rpmmacros} ${rpmmacros}_
touch ${rpmmacros}
echo "%_topdir ${topDir}">> ${rpmmacros}
echo "%_tmppath /tmp/$USER">> ${rpmmacros}
echo "%_maindir ${mainTopDir}">> ${rpmmacros}
# gpg rpm signing related:
echo "%_signature gpg">> ${rpmmacros}
echo "%_gpg_name  cernFrontier">> ${rpmmacros}
echo "%_gpg_path ${HOME}/.gnupg">> ${rpmmacros}
echo "%_gpgbin /usr/bin/gpg">> ${rpmmacros}

# If there's a src directory, start with no SOURCES
if [ -d ../src ]; then
	rm -rf ../SOURCES/*
fi

# Start with no BUILD files
rm -rf ../BUILD/*

# Call rpm specific build script (if any)
if [ -f ./rpmbuild_.sh ]; then
	./rpmbuild_.sh
fi

# Build the tar file if there's an src directory
if [ -d ../src ]; then
	cd ../src
	tar --exclude=.git -zhcf ../SOURCES/${Name}.tar.gz *
fi

# rpmbuild
cd ../SPECS
shift

set +x
echo "Output is going to build.log"
LOG=../scripts/build.log
>$LOG
tail -f $LOG &
LOGPID=$!
export HOME=${rpmmacrosDir}

# osVersion will be set to make an OS-specific RPM
if [ ! -z "$osVersion" ]; then
  nohup rpmbuild -ba $*  ${Name}.spec >>$LOG 2>&1 &
else
  nohup rpmbuild -ba $* --define 'dist %{nil}' ${Name}.spec >>$LOG 2>&1 &
fi
BUILDPID=$!

trap "set -x;kill $LOGPID;pkill -TERM -P $BUILDPID" 2
wait $BUILDPID
EC=$?
sleep 1

for RPM_NAME in `grep Wrote $LOG | grep rpm | awk '{print $2}'`
do
  RPM_LIST="$RPM_LIST $RPM_NAME"
done
test -z "$RPM_LIST" || rpmsign --addsign $RPM_LIST

# the following came from http://stackoverflow.com/questions/5719030/bash-silently-kill-background-function-process/5722850
# Some trickery to hide "Terminated" message
exec 3>&2          # 3 is now a copy of 2
exec 2> /dev/null  # 2 now points to /dev/null
kill $LOGPID
sleep 1            # sleep to wait for process to die
exec 2>&3          # restore stderr to saved
exec 3>&-          # close saved version
exit $EC
