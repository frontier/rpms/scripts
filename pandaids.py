#!/usr/bin/env python


from subprocess import *
import sys, os
def run_cmd(cmd):
	p = Popen(cmd, shell=True, stdout=PIPE)
	output = p.communicate()[0]
	return output

def print_(p):
	print eval('p'),':',eval(p)

pandaIds={}
def getPandaIds( fileName):
	global lineS, line , pandaId, id
	file=open( fileName)
	for i,line in enumerate(file):
		lineS=line.split('frontier-id')
		if len( lineS)>1 :
			pandaId_= lineS[1].split('[')
			if len(pandaId_)>1:
				pandaId= lineS[1].split('[')[1].split(']')[0]
				id= line.split('id=')[1].split()[0]
				pandaIds[ id]= pandaId
	file.close()
getPandaIds('./logs/part')

file=open("logAnalysisResults")
userTables={}
for line in file:
	lineS=line.split()
	id=lineS[0]
	userTable=lineS[6]+' '+ lineS[7]
	userTables[id]=userTable
file.close()

resultsFileName='idPandaIdUserTable'
results= open( resultsFileName, 'w')

file=open("COMP200.ids")
for line in file:
	id= line.strip()
	pandaId=pandaIds[id]
	cmd= "wget -q -O - \"http://panda.cern.ch/server/pandamon/query?job="+ str(pandaId)+"\"| grep prodUserName"
	pandaUser_= run_cmd( cmd)
	pandaUser= '"'+pandaUser_.split('</td><td>')[1].split('</td></tr>')[0]+'"'
	str_='id: '+str(id)+', pandaId: '+str(pandaId)+', pandaUser: '+str(pandaUser)+', DBuserTable: '+str(userTables[id])+'\n'
	#print_('str_') 
	results.write( str_)
file.close()
#./pandaids.py > idPandaId

#cmd='rm -f '+resultsFileName+'_;sort -k4 '+resultsFileName+'>'+resultsFileName+'_'
#print_('cmd')
#Fails. why?: os.system(cmd)
#column -t idPandaIdUserTable > idPandaIdUserTable_
