#!/bin/bash 
# Better source this file
# To be run from a .../rpms/<rpmSpecific>/scripts dir in order to be able
#    to find what rpm to refer to

RPMM=`pwd|awk -F /rpms/ '{print $2}'|awk -F / '{print $1}'`

WHICH_RPM=`echo $RPMM|cut -d- -f2-`
#echo "WHICH_RPM: $WHICH_RPM"
if [ Xfrontier-${WHICH_RPM} != X${RPMM} ] ; then # Not at an rpm dir, but rather at 'doc' or 'scripts' dir
		IS_RPM=0
		packageVersion=${RPMM}
else
		IS_RPM=1
		SPECFILE="../SPECS/${RPMM}.spec"
		#echo "SPECFILE: $SPECFILE"

		function specDefine2var(){
			eval "${1}="
			tempp_=`grep "^%define ${1} " ../SPECS/*.spec|awk '{print $3}'`
			#echo "$1 tempp_: $tempp_"
			if [ X${tempp_} != X ] ; then
				if [ ${tempp_} != %{nil} ]; then
					eval "${1}=${tempp_}"
				fi
			fi
		}

		# Assumption: ../SPEC has exactly one spec file
		function specKey2var(){
			tempp=`grep "^${1}:" ../SPECS/*.spec|awk '{print $2}'`
			questionMarkVars=`grep '{?' ../SPECS/*.spec|sed 's/%/\n%/g'|grep '{?'|sed 's/.*%{?//;s/}.*//'|sort -u`
			#echo $1 questionMarkVars: $questionMarkVars
			for questionMarkVar in $questionMarkVars
			do
				specDefine2var $questionMarkVar
			done
			for i in 1 2; do
			    # allow 2 levels of question mark vars
			    for questionMarkVar in $questionMarkVars
			    do
				    eval "evaledQuestionMarkVar=\$$questionMarkVar"
				    tempp=`echo $tempp|sed 's/%{?'$questionMarkVar'}/'$evaledQuestionMarkVar'/g'` 
			    done
			done	
			#echo "$1 tempp: $tempp"
			eval "${1}=${tempp}"
			#echo "\$1: $1"
		}

		specKey2var Name
		specKey2var Version
		specKey2var Release

		export BuildArch=
		specKey2var BuildArch
		if [ X${BuildArch} = X ] ; then
			BuildArch=`uname -m`
		fi
		export arch=$BuildArch

		packageVersion=${Name}-${Version}-${Release}
		#echo "packageVersion: ${packageVersion}"
fi
echo "packageVersion: $packageVersion"
