#!/bin/bash -x

rpm -e frontier-squid frontier-awstats frontier-tomcat

/bin/rm -rf /etc/squid /etc/tomcat /data/squid_cache /data/squid_logs /data/tomcat_logs

/usr/sbin/ncm_wrapper.sh --co dirperm;
/usr/sbin/ncm_wrapper.sh --co filecopy;
/usr/sbin/ncm_wrapper.sh --co sindes;
/usr/sbin/ncm_wrapper.sh --co vobox_sindes

rpm -ivh http://frontier.cern.ch/dist/rpms/RPMS/x86_64/frontier-squid-2.7.STABLE9-5.23.sl5.x86_64.rpm
rpm -ivh  http://frontier.cern.ch/dist/rpms/RPMS/noarch/frontier-tomcat-6.0.35_3.29-10.noarch.rpm
rpm -ivh http://frontier.cern.ch/dist/rpms/RPMS/noarch/frontier-awstats-6.0-16.noarch.rpm

/usr/sbin/ncm_wrapper.sh --co -all

# Testing
sleep 2
/afs/cern.ch/user/d/dfront/public/testSquidt0.sh 
/afs/cern.ch/user/d/dfront/public/testFrontiert0

